#ifndef CONNECTIONWORKER_H
#define CONNECTIONWORKER_H

#include <QObject>
#include <QThread>
#include <QDebug>
#include <QTcpSocket>

static constexpr const char *TCP_HOST = "127.0.0.1";
static constexpr const int TCP_PORT = 4444;

class ConnectionWorker : public QThread
{
    Q_OBJECT

    QTcpSocket  __socket;
    void doSendCommand(const QString& command);
public:
    ConnectionWorker();
    ~ConnectionWorker();

public slots:
    void doConnect();
    void onReadyRead();
    void onReadyWrite();

protected:
    void run() override;

signals:
    void tryConnect();
    void onTryConnect(bool isOk);
    void sendCommand(const QString& command);
    void gotResponse(const QByteArray reponse);


};

#endif // CONNECTIONWORKER_H
