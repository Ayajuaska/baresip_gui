#ifndef CONTROLAPI_H
#define CONTROLAPI_H

#include <QObject>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QMessageBox>
#include "connectionworker.h"
#include "sipeventtype.h"



class ControlApi : public QObject
{
    Q_OBJECT
private:
    class Tokens {
    public:
        static constexpr const char *REGISTER_REQUEST = "regReq";
        static constexpr const char *UADEL_REQUEST = "uadelReq";

        enum T_TOKEN {
            UNKNOWN = -1,
            T_REGISTER,
            T_UADELETE,
        };
        static T_TOKEN resolve(const QString& token)
        {
            if (token == REGISTER_REQUEST) {
                return T_REGISTER;
            }

            if (token == UADEL_REQUEST) {
                return T_UADELETE;
            }

            return UNKNOWN;
        }

    };

    ConnectionWorker *__connectionWorker = nullptr;
    void __initSignals();

    void registerTokenCb(const QJsonDocument& response);

    void eventCb(const QJsonDocument& response);

    void deluserAgent(const QString& ua);

public:
    explicit ControlApi(QObject *parent = nullptr);
    ~ControlApi();

signals:
    void updateStatus(QString);
    void connectionChanged(bool isOk);
    void sipRegister(const QString& host, const QString& name, const QString& password);

    /**
     * @brief Signals for communicationg with RegisterSM
     */
    void sipRegistered();
    void sipRegistering();
    void sipUnregistered();

public slots:
    void onConnect(bool);
    void doRegister(const QString& host, const QString& name, const QString& password);
    void parseResponse(const QByteArray response);
};

#endif // CONTROLAPI_H
