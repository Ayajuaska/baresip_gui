#ifndef REGESTERSM_H
#define REGESTERSM_H

#include <QObject>
#include <QState>
#include <QStateMachine>

class RegesterSM : public QStateMachine
{
public:
    QState *unregistered = nullptr, *registered = nullptr, *registering = nullptr;
    RegesterSM(QObject*);
};

#endif // REGESTERSM_H
