#include "connectionworker.h"

ConnectionWorker::ConnectionWorker() : QThread(),
    __socket(this)
{
    connect(this, &ConnectionWorker::tryConnect, this, &ConnectionWorker::doConnect);
    connect(&__socket, &QTcpSocket::connected, this, [this] () { emit this->onTryConnect(true);});
    connect(&__socket, &QTcpSocket::disconnected, this, [this] () { emit this->onTryConnect(false); });
    connect(this, &ConnectionWorker::sendCommand, this, &ConnectionWorker::doSendCommand);
    connect(&__socket, &QTcpSocket::readyRead, this, &ConnectionWorker::onReadyRead);
    start(LowPriority);
}

ConnectionWorker::~ConnectionWorker()
{
    this->exit(0);
    __socket.close();
}

void ConnectionWorker::doConnect()
{
    if (__socket.state() != QTcpSocket::ConnectedState && __socket.state() != QTcpSocket::ConnectingState) {
        __socket.connectToHost(TCP_HOST, TCP_PORT);
    }
}

void ConnectionWorker::run()
{
    while(true) {
        if (__socket.state() != QTcpSocket::ConnectedState && __socket.state() != QTcpSocket::ConnectingState) {
            qDebug() << "Socket state is " << __socket.state();
            this->sleep(10);
            emit tryConnect();
        }
    }
}

void ConnectionWorker::doSendCommand(const QString &command)
{
    __socket.write(command.toLatin1());
}

void ConnectionWorker::onReadyRead()
{
    auto response_raw = __socket.readAll();
    if (response_raw.length() > 0) {
        QString response = QString(response_raw.toStdString().c_str());
        emit gotResponse(response_raw);
    }
}

void ConnectionWorker::onReadyWrite()
{
    ;
}

