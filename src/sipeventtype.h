#ifndef SIPEVENTTYPE_H
#define SIPEVENTTYPE_H
#include <QString>

class SipEventType
{
public:
    enum UA_EVENT {
        UA_EVENT_UNKNOWN = -1,
        UA_EVENT_REGISTERING,
        UA_EVENT_REGISTER_OK,
        UA_EVENT_REGISTER_FAIL,
        UA_EVENT_UNREGISTERING,
        UA_EVENT_MWI_NOTIFY,
        UA_EVENT_SHUTDOWN,
        UA_EVENT_EXIT,
        UA_EVENT_CALL_INCOMING,
        UA_EVENT_CALL_RINGING,
        UA_EVENT_CALL_PROGRESS,
        UA_EVENT_CALL_ESTABLISHED,
        UA_EVENT_CALL_CLOSED,
        UA_EVENT_CALL_TRANSFER,
        UA_EVENT_CALL_TRANSFER_FAILED,
        UA_EVENT_CALL_DTMF_START,
        UA_EVENT_CALL_DTMF_END,
        UA_EVENT_CALL_RTCP,
        UA_EVENT_CALL_MENC,
        UA_EVENT_VU_TX,
        UA_EVENT_VU_RX,
        UA_EVENT_AUDIO_ERROR,
    };
    SipEventType() = default;
    static UA_EVENT resolve(const QString &needle);
};

#endif // SIPEVENTTYPE_H
