#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>
#include "src/controlapi.h"
#include "src/regestersm.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    RegesterSM *registerSM = nullptr;
    void registerSMInit();
    ControlApi *__api = nullptr;
    Ui::MainWindow *ui;

signals:
    /**
     * @brief Signals for Sip Register State Machine
     */
    void unregistered();
    void registering();
    void registered();

public slots:
    void onConnect(bool isOk);
    void updateStatus(const QString&);

    void onRegisterButtonClick(bool checked = false);
};

#endif // MAINWINDOW_H
