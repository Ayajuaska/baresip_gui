#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->statusBar->showMessage("Loading ok. Connecting…");
    ui->centralWidget->setEnabled(false);
    __api = new ControlApi(this);
    connect(__api, &ControlApi::updateStatus, this, &MainWindow::updateStatus);
    connect(__api, &ControlApi::connectionChanged, this, &MainWindow::onConnect);

    connect(__api, SIGNAL(sipRegistered()), this, SIGNAL(registered()));
    connect(__api, SIGNAL(sipUnregistered()), this, SIGNAL(unregistered()));
    connect(__api, &ControlApi::sipRegistering, this, [=] () {
        emit registering();
        qDebug() << "Registering";
    });

    connect(ui->registerBtn, &QPushButton::clicked, this, &MainWindow::onRegisterButtonClick);
    registerSMInit();
}

void MainWindow::registerSMInit()
{
    if (registerSM) {
        delete registerSM;
    }
    registerSM = new RegesterSM(this);

    QObject *sender = this;
    registerSM->unregistered->addTransition(this, SIGNAL(registering()), registerSM->registering);
    registerSM->registering->addTransition(this, SIGNAL(registered()), registerSM->registered);
    registerSM->registering->addTransition(this, SIGNAL(unregistered()), registerSM->unregistered);
    registerSM->registered->addTransition(this, SIGNAL(unregistered()), registerSM->unregistered);
    registerSM->setInitialState(registerSM->unregistered);

    registerSM->registered->assignProperty(this->ui->registerBtn, "enabled", false);
    registerSM->registered->assignProperty(ui->unregisterBtn, "enabled", true);
    registerSM->registered->assignProperty(ui->regStatus, "checked", true);

    registerSM->unregistered->assignProperty(ui->registerBtn, "enabled", true);
    registerSM->unregistered->assignProperty(ui->unregisterBtn, "enabled", false);
    registerSM->unregistered->assignProperty(ui->regStatus, "checked", false);

    registerSM->registering->assignProperty(ui->registerBtn, "enabled", false);
    registerSM->registering->assignProperty(ui->unregisterBtn, "enabled", false);
    registerSM->registering->assignProperty(ui->regStatus, "checked", false);

    registerSM->start();
}

void MainWindow::updateStatus(const QString& status)
{
    this->statusBar()->showMessage(status);
}

void MainWindow::onConnect(bool isOk)
{
    ui->centralWidget->setEnabled(isOk);
}

void MainWindow::onRegisterButtonClick(bool checked)
{
    emit __api->sipRegister(ui->hostIn->text(), ui->userIn->text(), ui->passIn->text());
}

MainWindow::~MainWindow()
{
    delete  __api;
    delete ui;
}
