#include "sipeventtype.h"

SipEventType::UA_EVENT SipEventType::resolve(const QString &needle)
{
    if (needle == "REGISTERING") { return UA_EVENT_REGISTERING; }
    else if (needle == "REGISTER_OK") { return UA_EVENT_REGISTER_OK; }
    else if (needle == "REGISTER_FAIL") { return UA_EVENT_REGISTER_FAIL; }
    else if (needle == "UNREGISTERING") { return UA_EVENT_UNREGISTERING; }
    else if (needle == "MWI_NOTIFY") { return UA_EVENT_MWI_NOTIFY; }
    else if (needle == "SHUTDOWN") { return UA_EVENT_SHUTDOWN; }
    else if (needle == "EXIT") { return UA_EVENT_EXIT; }
    else if (needle == "CALL_INCOMING") { return UA_EVENT_CALL_INCOMING; }
    else if (needle == "CALL_RINGING") { return UA_EVENT_CALL_RINGING; }
    else if (needle == "CALL_PROGRESS") { return UA_EVENT_CALL_PROGRESS; }
    else if (needle == "CALL_ESTABLISHED") { return UA_EVENT_CALL_ESTABLISHED; }
    else if (needle == "CALL_CLOSED") { return UA_EVENT_CALL_CLOSED; }
    else if (needle == "TRANSFER") { return UA_EVENT_CALL_TRANSFER; }
    else if (needle == "TRANSFER_FAILED") { return UA_EVENT_CALL_TRANSFER_FAILED; }
    else if (needle == "CALL_DTMF_START") { return UA_EVENT_CALL_DTMF_START; }
    else if (needle == "CALL_DTMF_END") { return UA_EVENT_CALL_DTMF_END; }
    else if (needle == "CALL_RTCP") { return UA_EVENT_CALL_RTCP; }
    else if (needle == "CALL_MENC") { return UA_EVENT_CALL_MENC; }
    else if (needle == "VU_TX_REPORT") { return UA_EVENT_VU_TX; }
    else if (needle == "VU_RX_REPORT") { return UA_EVENT_VU_RX; }
    else if (needle == "AUDIO_ERROR") { return UA_EVENT_AUDIO_ERROR; }
    return UA_EVENT_UNKNOWN;
}
