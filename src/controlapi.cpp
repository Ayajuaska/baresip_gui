#include "controlapi.h"

ControlApi::ControlApi(QObject *parent) : QObject(parent)
{
    __connectionWorker = new ConnectionWorker();
    __initSignals();
    __connectionWorker->doConnect();
}

void ControlApi::__initSignals()
{
    connect(__connectionWorker, SIGNAL(onTryConnect(bool)), this, SLOT(onConnect(bool)));
    connect(this, &ControlApi::sipRegister, this, &ControlApi::doRegister);
    connect(__connectionWorker, &ConnectionWorker::gotResponse, this, &ControlApi::parseResponse);
}

ControlApi::~ControlApi()
{
//    __connectionWorker->wait();
    delete __connectionWorker;
}

/**
 * @brief ControlApi::onConnect - получение статуса подключения к baresip
 * @param isOk - true - подключены, false - соответственно, нет
 */
void ControlApi::onConnect(bool isOk)
{
    QString message;
    if (isOk) {
        message = "Connected";
    } else {
        message = "Disconnected";
    }
    emit updateStatus(message);
    emit connectionChanged(isOk);
}

/**
 * @brief ControlApi::doRegister - формирование и отправка команды регистрации
 * @param host - хост, на котором регистрируемся
 * @param name - имя пользователя
 * @param password - пароль
 */
void ControlApi::doRegister(const QString& host, const QString& name, const QString& password)
{
    QJsonDocument commandJson;
    QJsonObject cmdObj;
    cmdObj.insert("command", "uanew");
    cmdObj.insert("params", QString("<sip:%1@%2>;auth_pass=%3").arg(host).arg(name).arg(password));
    cmdObj.insert("token", Tokens::REGISTER_REQUEST);
    commandJson.setObject(cmdObj);
    QString command = QString("%1:%2,")
            .arg(commandJson.toJson(QJsonDocument::Compact).length())
            .arg(QString(commandJson.toJson(QJsonDocument::Compact)));
    emit sipRegistering();
    emit __connectionWorker->sendCommand(command);
}

/**
 * @brief ControlApi::parseResponse - разбор ответа от baresip
 * @param response - ответ
 */
void ControlApi::parseResponse(const QByteArray response)
{
    int length = 0;
    for (auto i : response) {
        if (i < '0' || i > '9') {
            break;
        }
        unsigned char n = i - '0';
        length = length * 10 + n;
    }
    if (length < 1 ) {
        emit sipUnregistered();
        return;
    }
    auto doc = QJsonDocument::fromJson(response.mid(response.indexOf(':') + 1, length));
    qDebug() << doc;
    if (doc.object().find("event") != doc.object().end()) {
        return eventCb(doc);
    }
    switch (Tokens::resolve(doc.object().value("token").toString(""))) {
        default:
            qDebug() << QString(doc.toJson(QJsonDocument::Indented));
            break;
    }
}

/**
 * @brief ControlApi::registerTokenCb callback для ответов с токеном regReq (Запрос на регистрацию)
 * @param response
 */
void ControlApi::registerTokenCb(const QJsonDocument &response)
{
    qDebug() << "Registration status is " << response["ok"].toBool();
    if (response["ok"].toBool() != true) {
        emit sipUnregistered();
        qDebug() << "Register Fail";
        qDebug() << response["data"].toString();
    } else {
        emit sipRegistered();
        qDebug() << "Register OK";
    }
}

/**
 * @brief ControlApi::eventCb - callback для сообщений типа event
 * @param response
 */
void ControlApi::eventCb(const QJsonDocument& response)
{
    qDebug() << response;
    QJsonObject robj = response.object();
    QString eventType = robj.value("type").toString();

    if (eventType.length() < 1) {
        return;
    }
    switch (SipEventType::resolve(eventType)) {
        case SipEventType::UA_EVENT_REGISTERING:
            emit sipRegistering();
            break;
        case SipEventType::UA_EVENT_REGISTER_FAIL:
            emit sipRegistering();
            deluserAgent(robj.value("accountaor").toString());
            break;
        default:
            break;

    }
}

void ControlApi::deluserAgent(const QString &ua)
{
    QJsonDocument commandJson;
    QJsonObject cmdObj;
    cmdObj.insert("command", "uadel");
    cmdObj.insert("params", ua);
    cmdObj.insert("token", Tokens::UADEL_REQUEST);
    commandJson.setObject(cmdObj);
    QString command = QString("%1:%2,")
            .arg(commandJson.toJson(QJsonDocument::Compact).length())
            .arg(QString(commandJson.toJson(QJsonDocument::Compact)));
    emit __connectionWorker->sendCommand(command);
}
