#include "regestersm.h"

RegesterSM::RegesterSM(QObject *parent): QStateMachine (parent)
{
    unregistered = new QState(this);
    registered = new QState(this);
    registering = new QState(this);
}
